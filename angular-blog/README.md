# AngularBlog Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.1.

## Frontend

Na pasta raiz, abra um prompt do ms-dos e execute o comando npm install

Rode o comando `ng serve` para iniciar o serviço.

O serviço estará disponível em [http://localhost:4200] (http://localhost:4200).

## Backend
Instale o pacote json-server -> npm install -g json-server
Na pasta raiz, abra um prompt do ms-dos e execute o comando json-server db.json

O serviço estará disponível em [http://localhost:3000/messages] (http://localhost:3000/messages).
