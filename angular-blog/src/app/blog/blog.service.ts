import { Injectable } from '@angular/core'
import { Http, Headers, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { ActivatedRoute } from '@angular/router'

import { BLOG_API } from '../app.api'
import { ErrorHandle } from '../app.error-handler'
import { Message } from './messages/message.model'

@Injectable()
export class BlogService {
  private url: string = `${BLOG_API}/messages`

  constructor(private http: Http, private route: ActivatedRoute) { }

  messages(): Observable<Message[]> {
    return this.http.get(this.url)
      .map(response => response.json())
      .catch(ErrorHandle.handleError)
  }

  getMessage(id) {
    return this.http.get(this.getMessageUrl(id))
      .map(response => response.json())
      .catch(ErrorHandle.handleError)
  }

  addMessage(msg: Message): Observable<string> {
    console.log(JSON.stringify(msg))
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')
    return this.http.post(this.url, JSON.stringify(msg),
      new RequestOptions({ headers: headers }))
      .map(response => response.json())
      .map(msg => msg.id)
      .catch(ErrorHandle.handleError)
  }
  // addMessage(msg: Message): Observable<string> {
  //   let body = msg;
  //   let headers = new Headers({'Content-Type': 'application/json'});
  //   let options = new RequestOptions({headers: headers});
  //
  //   return this.http.post(this.url, body, options)
  //     .catch(ErrorHandle.handleError);
  // }

  updateMessage(msg: Message): Observable<string> {
    //console.log('Mensagem: ' + JSON.stringify(this.route.snapshot.params['id']))
    console.log('Mensagem: ' + JSON.stringify(msg))
    return this.http.put(this.getMessageUrl(msg.id), JSON.stringify(msg.message))
      .map(response => response.json())
      .map(msg => msg.id)
      .catch(ErrorHandle.handleError)
  }

  deleteMessage(id) {
    return this.http.delete(this.getMessageUrl(id))
      .map(response => response.json())
      .catch(ErrorHandle.handleError)
  }

  private getMessageUrl(id) {
    return this.url + "/" + id;
  }
}
