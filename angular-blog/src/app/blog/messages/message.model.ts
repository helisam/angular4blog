export class Message {
  id: number
  message: string
  constructor(
    message: string,
    id?: number
  ) {
    this.id = id
    this.message = message
  }
}
