import { Component, OnInit, Input } from '@angular/core';

import { Message } from './message.model'

@Component({
  selector: 'anb-message',
  templateUrl: './messages.component.html'
})
export class MessagesComponent implements OnInit {

  @Input() message: Message

  constructor() { }

  ngOnInit() {
  }

}
