import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Message } from './messages/message.model'
import { BlogService } from './blog.service'

@Component({
  selector: 'anb-blog',
  templateUrl: './blog.component.html'
})
export class BlogComponent implements OnInit {

  messages: Message[] = []
  private textAreaValue: string = ''
  private isEdit: boolean = false

  constructor(
    private blogService: BlogService,
    //formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.blogService.messages()
      .subscribe(messages => this.messages = messages)
  }

  deleteMessage(message) {
    //if (confirm("Are you sure you want to delete " + message.message + "?")) {
    var index = this.messages.indexOf(message);
    this.messages.splice(index, 1);

    this.blogService.deleteMessage(message.id)
      .subscribe(null,
      err => {
        alert("Could not delete message.");
        // Revert the view back to its original state
        this.messages.splice(index, 0, message);
      });
    //}
  }

  addMessage(message: string) {
    let msg: Message = new Message(message)
    this.blogService.addMessage(msg)
      .subscribe((id: string) => {
        console.log(id)
        this.blogService.messages()
          .subscribe(messages => this.messages = messages)
      })
  }

  updateMessage(msg: Message) {
    console.log(msg)
    this.isEdit = false
    //let msg: Message = new Message(message)
    this.blogService.updateMessage(msg)
      .subscribe((id: string) => {
        console.log('Id retorno: ' + id)
        this.blogService.messages()
          .subscribe(messages => this.messages = messages)
      })
  }

  editMessage(msg: Message) {
    console.log(msg)
    //msg.message =
    this.isEdit = true
  }
}
